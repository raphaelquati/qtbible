#ifndef PDBFILE_H
#define PDBFILE_H

#include <QtCore>
#include "PalmBible/PDBStructs.h"

class PDBFile
{
private:
    QFile *pdbfile;
    pdb_header header;
    pdb_rec_header *records;
    int lastSizeRead;

public:
    PDBFile();
    virtual ~PDBFile();

    int openPDB(QString filename);
    char* readRecord(int recNo);
    int getLastSizeRead();
};

#endif // PDBFILE_H
