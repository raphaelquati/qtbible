#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "QsKineticScroller.h"
#include "aboutwindow.h"

#include "versedelegate.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    setupKinect();
    setupSoftKeys();

    QUrl welcomeURL = QUrl("qrc:/static/Welcome.html");
    ui->textBrowser->setSource(welcomeURL);
    ui->statusBar->hide();

    //model = new ChapterModel(this);
    //ui->listView->setModel(model);

    //VerseDelegate *delegate = new VerseDelegate(this);
    //ui->listView->setItemDelegate(delegate);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_textBrowser_anchorClicked(QUrl url)
{
    //Navigate thru internal links, or
    //    thru commands (like Welcome)

    QString urlT = url.toString();

    if (url.toString().startsWith("action:")){
        urlT = urlT.remove(0, 7);

        if (urlT == "ImportPDB") {
            QUrl testURL = QUrl("qrc:/static/Test.html");
            ui->textBrowser->setSource(testURL);
        }
        if (urlT == "Back") {
            ui->textBrowser->backward();
        }
    } else {
        //TODO....
    }
}

void MainWindow::setupSoftKeys() {
    QAction* options = new QAction(tr("Options"), this);

    options->setSoftKeyRole(QAction::PositiveSoftKey);
    options->setMenu(ui->menuOptions);

    addAction(options);
}

void MainWindow::setupKinect() {
    QsKineticScroller *kinect = new QsKineticScroller(this);
    kinect->enableKineticScrollFor(ui->textBrowser);

    //QsKineticScroller *kinect2 = new QsKineticScroller(this);
    //kinect2->enableKineticScrollFor(ui->listView);

}

/*
void BatteryIndicator::addSoftKeys()
{
    ///////////////////////////////////////////////////////
    // Options Menu
    QAction* options = new QAction(tr("Test Select"), this);

    QMenu *menuOptions = new QMenu(this);
    menuOptions->addAction(tr("Test 1"), this, SLOT(Test1()));
    menuOptions->addAction(tr("Test 2"), this, SLOT(Test2()));
    options->setMenu(menuOptions);


    ///////////////////////////////////////////////////////
    // Exit Action
    QAction* exit = new QAction(tr("Test Exit"), this);
    connect(exit, SIGNAL(triggered()), this, SLOT(QuitApp()));

    #if defined(Q_OS_SYMBIAN)
        // Set Soft Key locations
        // QAction::PositiveSoftKey = Left Soft Key
        // This will set the left soft key menu
        options->setSoftKeyRole(QAction::PositiveSoftKey);
        exit->setSoftKeyRole(QAction::NegativeSoftKey);

        // Add Actions Direct to dialog
        // QAction::NegativeSoftKey = Right Soft Key
        // This will set the right softkey caption and action
        addAction(options);
        addAction(exit);
    #else
        // Create Menu Bar for the QDialog and add to that
        // Note:If you did this under symbian all the menus would be a sub menu of the
        //      Left Soft Key
        QMenuBar *menuBar = new QMenuBar(this);
        layout()->setMenuBar(menuBar);
        menuBar->addAction(options);
        menuBar->addAction(exit);
    #endif
}
*/

void MainWindow::on_actionAbout_triggered()
{
    AboutWindow *window = new AboutWindow(this);
    window->showMaximized();
}
