I have been attempting to port PB+ to Pocket PC, and I have tried to get a clear
understanding of the file format. Here is what I have so far.
Can someone let me know if I am on the right track.

Bible Plus+ File Structure Description
07 May 05 [RDM]

Initial Header
00 00 FileName (32 bytes padded with 0's)
00 20 FileAttributes (2 byte; init=24)
00 22 Version number (2 byte; init=1)
00 24 CreationDate (4 bytes)
00 28 ModificationDate (4 bytes)
00 2c LastBackupDate (4 bytes)
00 30 ModificationNumber (4 bytes; init=0)
00 34 AppInfoArea (4 bytes; init=0)
00 38 SortInfoArea (4 bytes; init=0)
00 3c DatabaseType (4 bytes; init="bibl")
00 40 CreatorID (4 bytes; init="PPBL")
00 44 UniqueIDSeed (4 bytes; init=0)
00 48 NextRecord (4 bytes; init=0)
00 4c NumRecs (2 byte; init=0)

Records List Jump Table (total size is 8*NumRecs+2 places you at VersionInfoType when added to 0x004e)
Records are 4096 bytes long (with apparently the exception of Record 36, which is 1083 bytes long)
00 4e 4 byte offset for location of VersionInfoType (a 152 byte structure organized as noted below) Record 0
00 56 4 byte offset for location of WordInfoType table Record 1
00 5e 4 byte offset for start of word table Record 2
00 66 Record 3, etc.


Version Type Information pointed to by the address at 0x004e and matches record 0
xx 00 VersionName (16 bytes)
xx 10 VersionInfo (128 bytes)
xx 90 SepChar (1 byte; init=" ")
xx 91 VersionAttrib (1 byte; init=2)
xx 92 WordIndex (2 bytes; init=1)
xx 94 TotalWordRec (2 bytes; as calcuated when the file is created)
xx 96 TotalBooks (2 bytes; init=66 [usually])

Based on the number of "TotalBooks" the following structure [BookInfoType] exists for each book and immediately follows VersionInfoType and matches Record 1
xx 00 BookNum (2 bytes; each book has a unique number)
xx 02 BookIndex (2 bytes; This points to the record number where the Book Index for that book is located)
xx 04 TotalBookRec (2 btyes; This contains the number of records used to hold the text which immediately follows the Book Index for that book)
xx 06 SimpleName (8 bytes; this is the name that appears on the jump menu(
xx 0d ComplexName (32 bytes; this is the full name)

Word Info Type Table (Immediately follows the Book Table above and it pointed to by address 0x0056 and matches Record 2)
xx 00 TotalIndexes (2 bytes; gives number of different word sizes)
Based on the TotalIndexes, there is one structure for each of the sizes up to the number indicated
xx 02 WordLength (2 bytes)
xx 04 TotalWord (2 bytes)
xx 06 Boolean compressed (1 byte)
xx 07 Boolean nothing (1 byte)

Word Dictionary Structure
Starting from the offsets indicated above, the word lists run continuously without spaces.
Record numbers occur at random locations within the word lists.
At record 28, offset 331, the word lists "end" and then word pairs are listed

Record number indicated for the first book is where the Book Index Records begin, and they are of variable length according to the following scheme:
Example: Genesis
2 bytes for the number of Chapters [NumChapters] (50 or 0x0032)
2 bytes times the number of chapters in the book to hold the number of verses in each chapter [LastVerseNumber] (2*50=100)
4 bytes times the number of chapters in order to hold the offsets for each chapter [ChapterOffset] (4*50=200)
2 bytes times the muber of verses in the book [VerseOffset] (2*1533=3066)
This makes this record length 3368 bytes (2+100+200+3066=3368)

Immediately following the Book Index Record is the actual text, and this starts with a new record number.
The BookInfoType structure indicates how many records of 4096 bytes are contained in a book.
Usually the last record will be less than 4096 bytes.
In the text area, each word or word pair is indicated by a 2 byte number offset from the start of Record 2.
Chapter and verse numbers are not contained in the text but must be calculated.
