#include "aboutwindow.h"
#include "ui_aboutwindow.h"
#include "QsKineticScroller.h"
#include "pdbfile.h"
#include "PalmBible/database.h"

AboutWindow::AboutWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::AboutWindow)
{
    ui->setupUi(this);

    QAction* dismiss = new QAction(tr("Ok"), this);
    dismiss->setSoftKeyRole(QAction::NegativeSoftKey);
    addAction(dismiss);

    connect(dismiss, SIGNAL(triggered()), this, SLOT(dismiss_triggered()));

    QsKineticScroller *kinect = new QsKineticScroller(this);
    kinect->enableKineticScrollFor(ui->textBrowser);
}

AboutWindow::~AboutWindow()
{
    delete ui;
}

void AboutWindow::dismiss_triggered() {
    PDBFile *pdb = new PDBFile();
    pdb->openPDB(":/Bible/NVI_Br.pdb");

    Database *db = new Database(pdb);


    db->getBookName();


    delete db;

    delete pdb;

    close();
}
