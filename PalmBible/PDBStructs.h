#ifndef PDBSTRUCTS_H
#define PDBSTRUCTS_H


#define _DWord unsigned int
#define _Word unsigned short

#define UInt32 unsigned int
#define UInt16 unsigned short
#define UInt8 unsigned char

struct pdb_header {       /* 78 bytes total */
          char	 name[ 32 ]; /*0*/
          _Word	 attributes; /* 32 */
          _Word	 version; /* 34 */
          _DWord	 create_time; /* 36 */
          _DWord	 modify_time; /* 40 */
          _DWord	 backup_time; /* 44 */
          _DWord	 modificationNumber; /* 48 */
          _DWord	 appInfoID; /* 52 */
          _DWord	 sortInfoID; /* 56*/
          char	 type[4]; /* 60 */
          char	 creator[4]; /* 64 */
          _DWord	 id_seed; /*68 */
          _DWord	 nextRecordList; /*72*/
          _Word	 numRecords; /* 76 */
}  /*__attribute__((packed)) */;


struct pdb_rec_header {   /* 8 bytes total	*/
          _DWord	 offset;
          char attr;
/*	  struct {
                 int deleted    : 1;
                 int dirty     : 1;
                 int busy      : 1;
                 int secret    : 1;
                 int category  : 4;
          }	 attributes;*/
          char	 uniqueID[3];
} /*__attribute__((packed))*/;

#endif // PDBSTRUCTS_H
