#ifndef DATABASE_H
#define DATABASE_H

#include "PalmBible/pb_types.h"
#include "pdbfile.h"
#include <QtCore>

class Database
{

public:
    Database(PDBFile *pdb);
    virtual ~Database();

    BookInfoType* getBookInfo();
    int getTotalBookCount();
    QString readVerse(int book, int chapter, int verse);
    QString getBookName(int book);
    QString getBookSimpleName(int book);


private:
    PDBFile *current_pdb;
    VersionInfoType versionType;
    BookInfoType *bookInfo;
    unsigned short TotalIndexes;
    unsigned int totalWords;
    WordIndexType *wordIndex;
    BookIndexType *currentBookIndex;

    QList<QString> words;
    UInt16 *wordIndexOffset;

    int getWordIndex(int pos, int wordNum);
    int getWordPos(int wordNum);

    void readBooksInfo();
    void readWordInfo();
    void readBookIndex(int bookNum);
    void readWords();
    char *RW_readRecord(int currentRecord, int *size);






};

#endif // DATABASE_H
