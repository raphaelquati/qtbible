#ifndef PB_TYPES_H
#define PB_TYPES_H

#include "PDBStructs.h"

#define VERSIONINFOREC 0
#define WORDINFOTYPEREC 1

typedef struct BookInfoType {
        UInt16 bookNum;
        UInt16 bookIndex;
        UInt16 totalBookRec;
        char simpleName[8];
        char complexName[32];
} BookInfoType;

typedef BookInfoType *BookInfoPtr;

typedef struct VersionInfoType {
        char versionName[16]; /*0 */
        char versionInfo[128]; /*16 */
        char sepChar;
        UInt8 versionAttrib; /**128+2+16= 130+16=146*/
        UInt16 wordIndex;
        UInt16 totalWordRec;
        UInt16 totalBooks; /*16*3+146 = 194*/
        //BookInfoType *bookInfo; // bookInfo array
} /*__attribute__((packed))*/ VersionInfoType;

typedef VersionInfoType *VersionInfoPtr;

typedef struct ListContainType {
                char **nameList;
                UInt16 *numList;
                UInt16 totalValue;
} /*__attribute__((packed))*/ ListContainType;
typedef ListContainType *ListContainPtr;

typedef struct BookIndexType {
        UInt16 NumChapters;
        UInt16 *ChapterVerseIndex;
        UInt32 *ChapterOffset;
        UInt16 *VerseOffset;
        UInt16 *rawBuffer;
        int rawBufferSize;
} BookIndexType;

typedef struct WordIndexType {
    _Word WordLength;
    _Word TotalWord;
    char compressed;
    char dummy;
} WordIndexType;

#endif // PB_TYPES_H
