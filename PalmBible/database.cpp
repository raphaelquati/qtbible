#include "database.h"
#include <QMessageBox>

Database::Database(PDBFile *pdb)
{
    current_pdb = pdb;
    bookInfo = NULL;
    wordIndex = NULL;
    currentBookIndex = NULL;

    readBooksInfo();
    readWordInfo();
    readWords();

    readBookIndex(0);


    readVerse(1,1,1);
//    //test read chars....
//    char *data = current_pdb->readRecord(versionType.wordIndex + 1);

//    delete data;

}

Database::~Database() {
    if (bookInfo)
        delete [] bookInfo;

    if (wordIndex) {
        delete [] wordIndex;
        delete [] wordIndexOffset;
    }

    if (currentBookIndex) {
        delete currentBookIndex->rawBuffer;
        delete [] currentBookIndex;
    }
}

int Database::getTotalBookCount() {
    return versionType.totalBooks;
}

BookInfoType* Database::getBookInfo() {
    return bookInfo;
}

void Database::readBooksInfo() {
    char *data;

    data = current_pdb->readRecord(VERSIONINFOREC);

    memcpy(&versionType, data, sizeof(versionType));

    //Fix members
    versionType.wordIndex = qFromBigEndian(versionType.wordIndex);
    versionType.totalWordRec = qFromBigEndian(versionType.totalWordRec);
    versionType.totalBooks = qFromBigEndian(versionType.totalBooks);

    bookInfo = new BookInfoType[versionType.totalBooks];

    for (int i  = 0 ; i < versionType.totalBooks; i++ ) {
        memcpy(&bookInfo[i], data + sizeof(versionType) + (i * sizeof(BookInfoType)), sizeof(BookInfoType));

        bookInfo[i].bookNum = qFromBigEndian(bookInfo[i].bookNum);
        bookInfo[i].bookIndex = qFromBigEndian(bookInfo[i].bookIndex);
        bookInfo[i].totalBookRec = qFromBigEndian(bookInfo[i].totalBookRec);
    }

    delete data;
}

void Database::readBookIndex(int bookNum) {
    currentBookIndex = new BookIndexType();

    currentBookIndex->rawBuffer = (UInt16*) current_pdb->readRecord(bookInfo[bookNum].bookIndex);
    currentBookIndex->rawBufferSize = current_pdb->getLastSizeRead();


    /* totalChapters */
    currentBookIndex->NumChapters = qFromBigEndian(currentBookIndex->rawBuffer[0]);

    /* totalVersesAcc */
    currentBookIndex->ChapterVerseIndex = &currentBookIndex->rawBuffer[1];

    /* totalChapterCharsAcc */
    currentBookIndex->ChapterOffset = (UInt32*) &currentBookIndex->rawBuffer[currentBookIndex->NumChapters + 1];

    /* totalVerseCharsAcc */
    currentBookIndex->VerseOffset = &currentBookIndex->rawBuffer[currentBookIndex->NumChapters * 3 + 1];


    /* Fix Members */
    for (int i = 0; i < currentBookIndex->NumChapters; i++) {
        currentBookIndex->ChapterVerseIndex[i] = qFromBigEndian(currentBookIndex->ChapterVerseIndex[i]);
        currentBookIndex->ChapterOffset[i] = qFromBigEndian(currentBookIndex->ChapterOffset[i]);
    }

    int sizeOfVerseOffset = (currentBookIndex->rawBufferSize / sizeof(UInt16)) - (currentBookIndex->NumChapters * 3 + 1) ;
    for (int i = 0; i < sizeOfVerseOffset; i++) {
        currentBookIndex->VerseOffset[i] = qFromBigEndian(currentBookIndex->VerseOffset[i]);
    }


//    char msg[256];
//    sprintf(msg, "%s - %d chapters.", bookInfo[bookNum].complexName, bookIndex->NumChapters);

//    QMessageBox msgBox;
//    msgBox.setText(msg);
//    msgBox.exec();
}


void Database::readWordInfo() {
    char *data;

    data = current_pdb->readRecord(versionType.wordIndex);

    memcpy(&TotalIndexes, data, sizeof(TotalIndexes));
    TotalIndexes = qFromBigEndian(TotalIndexes);

    wordIndex = new WordIndexType[TotalIndexes];
    long totalDicSize = 0;

    wordIndexOffset = new UInt16[TotalIndexes];

    totalWords = 0;

    wordIndexOffset[0] = 0;
    for (int i = 0; i < TotalIndexes; i++) {
        memcpy(&wordIndex[i], data + 2 + (i * sizeof(WordIndexType)), sizeof(WordIndexType));

        wordIndex[i].WordLength = qFromBigEndian(wordIndex[i].WordLength);
        wordIndex[i].TotalWord = qFromBigEndian(wordIndex[i].TotalWord);

        if (i > 0)
            wordIndexOffset[i] = totalDicSize;

        totalDicSize += (wordIndex[i].TotalWord * wordIndex[i].WordLength);
        totalWords += wordIndex[i].TotalWord;

    }

    delete data;
}

char *Database::RW_readRecord(int currentRecord, int *size) {
    char *ret;

    ret = current_pdb->readRecord(currentRecord);
    (*size) = current_pdb->getLastSizeRead();

    return ret;
}

void Database::readWords() {
    int dataPointer = 0;

    char *data;
    int sizeData;

    QString word;
    int currentRecord = versionType.wordIndex + 1;

    data = current_pdb->readRecord(currentRecord);
    sizeData = current_pdb->getLastSizeRead();

    for (int i = 0; i < TotalIndexes; i++) {
        //cword = (char *) malloc(wordIndex[i].WordLength + 1);

        for (int n = 0; n < wordIndex[i].TotalWord; n++) {
            word.clear();

            if (dataPointer + wordIndex[i].WordLength > sizeData) {
                //Word incomplete.

                //strncpy(cword, data + (dataPointer - sizeData), wordIndex[i].WordLength);
                word = QString::fromAscii(data + dataPointer, sizeData - dataPointer);

                dataPointer = 0; delete data; currentRecord++;
                data = RW_readRecord(currentRecord, &sizeData);

                dataPointer += (wordIndex[i].WordLength - word.length());

                //strncpy(cword, data + (dataPointer - sizeData), wordIndex[i].WordLength);
                word.append(QString::fromAscii(data, wordIndex[i].WordLength - word.length()));
            } else {
                //strncpy(cword, data + dataPointer, wordIndex[i].WordLength);

                word = QString::fromAscii(data + dataPointer, wordIndex[i].WordLength);
                dataPointer += wordIndex[i].WordLength;
            }

            words << word;

            if (dataPointer > sizeData) {
                dataPointer = 0; delete data; currentRecord++;
                data = RW_readRecord(currentRecord, &sizeData);
            }
        }

        //free(cword);

        if (dataPointer > sizeData) {
            dataPointer = 0; delete data; currentRecord++;
            data = RW_readRecord(currentRecord, &sizeData);
        }
    }
}


int Database::getWordPos(int wordNum) {
    int relNum = wordNum - 1;

    for (int i=0; i< TotalIndexes; i++){

        int _totalWord = wordIndex[i].TotalWord;

        if (relNum < _totalWord){
            return i;
        } else {
            relNum = (relNum - _totalWord);
        }
    }
    return 0;
}

int Database::getWordIndex(int pos, int wordNum) {
    int relNum = wordNum - 1;
    int decWordIndex = 0;
    for (int i=0; i<=pos; i++){
        int _totalWord = wordIndex[i].TotalWord;

        if (relNum < _totalWord){
            int decWordLen = wordIndex[i].WordLength;

            decWordIndex = wordIndexOffset[i] + relNum * decWordLen;
            break;
        } else {
            relNum = (relNum - _totalWord);
        }
    }
    return decWordIndex;
}

QString Database::readVerse(int book, int chapter, int verse)
{
    /*
    Para achar um versiculo:

    x = ChapterVerseIndex[capitulo - 1] + numvers
    y = ChapterOffset[capitulo]

    ixVers = VerseOffset[x - 1] (verseStart)
    lenVers = VerseOffset[x] - VerseOffset[x - 1] (verseLength)

    (Decoder)
     se !byteShifted
     start = ixVers * 2

     recordIndex = start / recordSize (????)
     charIndex = (UInt16) (start % recordSize);

     minRecord = bookIndex[x].bookIndex;

     //Le pdb (minRecord + recordIndex)
    */

    UInt32 verseAcc = currentBookIndex[book-1].ChapterVerseIndex[chapter - 1] + verse;
    UInt32 verseStart = currentBookIndex[book-1].ChapterOffset[chapter];
    UInt16 verseLength;
    if (verse > 1) {
        verseStart += currentBookIndex[book-1].VerseOffset[verseAcc - 1];
        verseLength = currentBookIndex[book-1].VerseOffset[verseAcc] - currentBookIndex[book-1].VerseOffset[verseAcc - 1];
    } else {
        verseLength = currentBookIndex[book-1].VerseOffset[verseAcc];
    }

    UInt32 compStart = verseStart * 2;

    /* ReadBegin(compStart) */

    UInt16 recordIndex = (UInt16) (compStart / 4096);
    UInt16 charIndex = (UInt16) (compStart % 4096);
    int minRecord = bookInfo[book-1].bookIndex;

    UInt16 *data = (UInt16 *) current_pdb->readRecord(
                bookInfo[book-1].bookIndex + 1 +
                recordIndex);

    /* */

    //int decWordNum = (data[0] * 256) + data[1];
    int decWordNum = qFromBigEndian(data[0]);

    int pos = getWordPos(decWordNum);
    int wi = getWordIndex(pos, decWordNum);

    delete data;

    return "";
}

QString Database::getBookName(int book)
{
    return QString(bookInfo[book].complexName);
}

QString Database::getBookSimpleName(int book)
{
    return QString(bookInfo[book].simpleName);
}

