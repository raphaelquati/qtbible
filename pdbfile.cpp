#include "pdbfile.h"
#include "PalmBible/PDBStructs.h"
#include <QByteArray>

PDBFile::PDBFile()
{
    pdbfile = NULL;
    records = NULL;
}

PDBFile::~PDBFile() {
    if (pdbfile) {
        pdbfile->close();
        delete pdbfile;
    }

    if (records)
        delete [] records;
}


int PDBFile::openPDB(QString filename) {
    if (pdbfile) {
        pdbfile->close();
        delete pdbfile;
    }

    pdbfile = new QFile(filename);

    if (!pdbfile->exists()) {
        delete pdbfile;
        return -1; //TODO better error handling
    }

    pdbfile->open(QIODevice::ReadOnly);

    char data[78];
    pdbfile->read(data, 78);

    memcpy(&header, data, 78);

    //Fix useful members
    header.numRecords = qFromBigEndian(header.numRecords);

    records = new pdb_rec_header[header.numRecords];
    memset(records, 0, sizeof(pdb_rec_header) * header.numRecords);

    return 0;
}

char* PDBFile::readRecord(int recNo) {
    int size = 0;

    lastSizeRead = 0;

    //Record not read yet.
    if (records[recNo].offset == 0) {

        pdbfile->seek(78 + (recNo * sizeof(pdb_rec_header)));

        pdbfile->read((char *) &records[recNo], sizeof(pdb_rec_header));

        //Fix members
        records[recNo].offset = qFromBigEndian(records[recNo].offset);
    }

    //PDB record information does not have size of the record. So we need to
    //   read the next record to compute the size (nextoffset - offset)
    if (recNo < header.numRecords - 1) {
        if (records[recNo+1].offset == 0) {
            pdbfile->seek(78 + ((recNo+1) * sizeof(pdb_rec_header)));
            pdbfile->read((char *) &records[recNo + 1], sizeof(pdb_rec_header));

            records[recNo + 1].offset = qFromBigEndian(records[recNo + 1].offset);
        }

        size = records[recNo + 1].offset - records[recNo].offset;
    } else {
        size = pdbfile->size() - records[recNo].offset;
    }

    pdbfile->seek(records[recNo].offset);
    lastSizeRead = size;

    char* content = new char[size];

    pdbfile->read(content, size);

    return content;
}

int PDBFile::getLastSizeRead() {
    return lastSizeRead;
}
