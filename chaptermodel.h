#ifndef CHAPTERMODEL_H
#define CHAPTERMODEL_H

#include <QAbstractListModel>

#include "PalmBible/database.h"
#include "verse.h"

QT_BEGIN_NAMESPACE
class QAbstractItemModel;
class QObject;
QT_END_NAMESPACE

class ChapterModel : public QAbstractListModel
{
    Q_OBJECT

public:
    ChapterModel(QObject *parent = 0);

    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    int columnCount(const QModelIndex &parent) const;
    QVariant headerData(int section, Qt::Orientation orientation, int role) const;
    QVariant data(const QModelIndex &index, int role) const;

    void initialize(Database *database);
    void setChapterNum(int num);
    void setBookNum(int num);

protected:
    bool canFetchMore(const QModelIndex &parent) const;
    void fetchMore(const QModelIndex &parent);

    //See examples/4.6/itemviews/fetchmore
private:
    QString bookName;

    mutable QHash<QModelIndex, Verse*> cache;

signals:

public slots:

};

#endif // CHAPTERMODEL_H
