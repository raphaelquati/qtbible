#include "chaptermodel.h"

ChapterModel::ChapterModel(QObject *parent) :
    QAbstractListModel(parent)
{

}

QVariant ChapterModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid() || role != Qt::DisplayRole)
        return QVariant();


    Verse *verse = new Verse();

    verse->num = index.row();
    verse->text = "No princípio";

    if (cache.contains(index)) {
        verse = cache.value(index);
    } else {
        //Ler do PDB o verso!
        cache.insert(index, verse);
    }

    //if (index.column() < data.count())
    return qVariantFromValue(verse);
}

int ChapterModel::rowCount(const QModelIndex &parent) const {
    return 100;
}

int ChapterModel::columnCount(const QModelIndex &parent) const {
    return 1;
}

QVariant ChapterModel::headerData(int section, Qt::Orientation orientation, int role) const {
    return QVariant();
}

void ChapterModel::initialize(Database *database) {

}

void ChapterModel::setChapterNum(int num) {

}

void ChapterModel::setBookNum(int num) {

}

bool ChapterModel::canFetchMore(const QModelIndex &parent) const {
    if (cache.count() < 100)
        return true;
    else
        return false;
}

void ChapterModel::fetchMore(const QModelIndex &parent) {
    //Do Nothing yet
}

