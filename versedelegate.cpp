#include "versedelegate.h"

#include <QtGui>
#include <QLabel>

VerseDelegate::VerseDelegate(QObject *parent) :
    QAbstractItemDelegate(parent)
{
    verseHeight = QSize(100,32);
}


void VerseDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option,
                          const QModelIndex &index) const {

    if (option.state & QStyle::State_Selected)
        painter->fillRect(option.rect, option.palette.highlight());

    int size = qMin(option.rect.width(), option.rect.height());

    int brightness = index.model()->data(index, Qt::DisplayRole).toInt();

    double radius = (size/2.0) - (brightness/255.0 * size/2.0);

    if (radius == 0.0)
        return;


    painter->save();
    painter->setRenderHint(QPainter::Antialiasing, true);
    painter->setPen(Qt::NoPen);
    if (option.state & QStyle::State_Selected)
        painter->setBrush(option.palette.highlightedText());
    else
        painter->setBrush(option.palette.text());

    Verse *verse = index.model()->data(index, Qt::DisplayRole).value<Verse *>();

    QLabel l;
    l.setText(QString("Verso %1").arg(verse->num));
    l.render(painter, QPoint(option.rect.x(), option.rect.y()));

    //QApplication::style()->drawControl(QStyle::CE_ProgressBar,&progressBarOption, painter);

    painter->setPen(Qt::white);
    painter->drawText(option.rect, verse->text);

    painter->restore();


}

QSize VerseDelegate::sizeHint(const QStyleOptionViewItem &option,
                              const QModelIndex &index ) const {

    return verseHeight;
}

