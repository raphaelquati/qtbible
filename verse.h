#ifndef VERSE_H
#define VERSE_H

#include <QtCore>

typedef struct Verse
{
    int num;
    QString comment;
    QString text;
} Verse;

Q_DECLARE_METATYPE(Verse);
Q_DECLARE_METATYPE(Verse*);


#endif // VERSE_H
