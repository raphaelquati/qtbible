#ifndef VERSEDELEGATE_H
#define VERSEDELEGATE_H

#include <QAbstractItemDelegate>
#include <QFontMetrics>
#include <QModelIndex>
#include <QSize>
#include "chaptermodel.h"

QT_BEGIN_NAMESPACE
class QAbstractItemModel;
class QObject;
class QPainter;
QT_END_NAMESPACE

class VerseDelegate : public QAbstractItemDelegate
{
    Q_OBJECT

public:
    explicit VerseDelegate(QObject *parent = 0);

    void paint(QPainter *painter, const QStyleOptionViewItem &option,
                   const QModelIndex &index) const;

    QSize sizeHint(const QStyleOptionViewItem &option,
                       const QModelIndex &index ) const;

signals:

public slots:

private:
    QSize verseHeight;

};

#endif // VERSEDELEGATE_H
