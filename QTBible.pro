#-------------------------------------------------
#
# Project created by QtCreator 2011-02-07T21:00:21
#
#-------------------------------------------------

QT       += core gui

TARGET = QTBible
TEMPLATE = app

SOURCES += main.cpp \
     mainwindow.cpp \
    QsKineticScroller.cpp \
    aboutwindow.cpp \
    pdbfile.cpp \
    PalmBible/database.cpp \
    chaptermodel.cpp \
    versedelegate.cpp

HEADERS  += \
        mainwindow.h \
    QsKineticScroller.h \
    aboutwindow.h \
    pdbfile.h \
    PalmBible/database.h \
    PalmBible/PDBStructs.h \
    PalmBible/pb_types.h \
    chaptermodel.h \
    verse.h \
    versedelegate.h

FORMS    += \
        mainwindow.ui \
    aboutwindow.ui


OTHER_FILES += \
    Welcome.html \
    Notes.txt \
    Test.html

RESOURCES += \
    resources.qrc
