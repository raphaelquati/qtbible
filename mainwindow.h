#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QUrl>

#include "chaptermodel.h"

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    void setupSoftKeys();
    void setupKinect();

    ChapterModel *model;

private slots:
    void on_actionAbout_triggered();
    void on_textBrowser_anchorClicked(QUrl url);
};

#endif // MAINWINDOW_H
